ProsePoint CMS - A newspaper content management system built as a Drupal distribution. 

The download for ProsePoint CMS is kept offsite at the ProsePoint website. ProsePoint requires third-party software components, and it is drupal.org policy not to host third-party code.

Please visit http://www.prosepoint.org to download ProsePoint or try the demo. 
